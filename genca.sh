#!/bin/bash
read -p "请输入要签名的IP:" ip
if [ ! -d "cert/$ip" ];then
  #echo "不存在"
  mkdir -p cert/$ip
else
  #echo "存在"
  rm -rf cert/$ip/*
fi
cd cert/$ip


# CA根证书请求主体部分
cat > root-ca.txt <<-EOF
[req]
distinguished_name = req_distinguished_name
prompt = no

[req_distinguished_name]
C= CN
ST = 湖南省
L = 长沙市
O = Starlet Studio
OU = R&D depts
CN = Starlet DevOps Root CA

EOF

# Server证书请求主体部分
cat > server-ca.txt <<-EOF
[req]
distinguished_name = req_distinguished_name
prompt = no

[req_distinguished_name]
C= CN
ST = 湖南省
L = 长沙市
O = Starlet Studio
OU = Harbor 专用证书
CN = $ip

EOF

# CA 根证书用于分发给操作系统
#ca generate
openssl genrsa -out ca.key 4096
#openssl req -new -key ca.key -out ca.csr

openssl req  -x509 -new -nodes -sha512 -days 3650 \
 -utf8 -config root-ca.txt \
 -key ca.key \
 -out ca.crt


# server cert generate
openssl genrsa -out $ip.key 4096
openssl req -sha512 -new \
    -config server-ca.txt -utf8\
    -key $ip.key \
    -out $ip.csr

cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
IP.0 = $ip
IP.1 = 10.10.10.20

EOF

openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -in $ip.csr \
    -out $ip.crt
#清除多余的文件
rm -rf server-ca.txt root-ca.txt v3.ext

#生成x509 格式的cert
openssl x509 -inform PEM -in $ip.crt -out $ip.cert

#生成pfx证书，** pfx 导出在 git-bash 中无法工作，windows平台下请使用bash.exe(WSL)**
#openssl pkcs12 -export -in $ip.crt -inkey $ip.key -certfile ca.crt -out $ip.pfx



